# xivo-web-jitsi

Web container of Jitsi environment.

## Web App Customization

To customize jitsi web interface override 
- in docker/web/custom-config.js the ``config`` object
- in docker/web/custom-interface_config.js the ``interfaceConfig`` object

The config in the running container is generated at startup from several files see [10-config script](https://github.com/jitsi/docker-jitsi-meet/blob/stable-5870/web/rootfs/etc/cont-init.d/10-config)

To find all options, please refer to:
- jitsi web docker templates:
  - [config.js system tpl](https://github.com/jitsi/docker-jitsi-meet/blob/stable-5870/web/rootfs/defaults/system-config.js)
  - [config.js settings tpl](https://github.com/jitsi/docker-jitsi-meet/blob/stable-5870/web/rootfs/defaults/settings-config.js)
- jitsi web app files:
  - [config.js](https://github.com/jitsi/jitsi-meet/blob/stable/jitsi-meet_5870/config.js)
  - [interface_config.js](https://github.com/jitsi/jitsi-meet/blob/stable/jitsi-meet_5870/interface_config.js)