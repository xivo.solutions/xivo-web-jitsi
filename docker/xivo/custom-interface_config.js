// XiVO Custom settings
interfaceConfig.APP_NAME = 'Wisper/XiVO Visio powered by Jitsi';
interfaceConfig.DEFAULT_REMOTE_DISPLAY_NAME = 'Fellow User';
interfaceConfig.DEFAULT_LOGO_URL = 'images/xivo.svg';
interfaceConfig.JITSI_WATERMARK_LINK = 'https://xivo.solutions';
interfaceConfig.SUPPORT_URL = 'https://support.xivo.solutions';
interfaceConfig.ENFORCE_NOTIFICATION_AUTO_DISMISS_TIMEOUT = 2000;